package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uzb.farhod.lesson6_2_bankomat_system.entity.Monitoring;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface MonitoringRepository extends JpaRepository<Monitoring, Integer> {
    @Query(value = "select sum(k.amount*m.kupyura_count) from monitoring m join kupyura k on m.kupyura_id = k.id " +
            "where date(m.time)= :date and m.bankomat_id= :bankomatId and m.user_action='KARTAGA_PUL_SOLISH'" , nativeQuery = true)
    Double getDailyIncome(@Param("date") Date date, @Param("bankomatId") Integer bankomatId);

    @Query(value = "select sum(k.amount*m.kupyura_count) from monitoring m join kupyura k on m.kupyura_id = k.id " +
            "where date(m.time)= :date and m.bankomat_id= :bankomatId and m.user_action='NAQD_PUL_YECHISH'" , nativeQuery = true)
    Double getDailyOutcome(@Param("date") Date date, @Param("bankomatId") Integer bankomatId);

    @Query(value = "select sum(k.amount*m.kupyura_count) from monitoring m join kupyura k on m.kupyura_id = k.id " +
            "where date(m.time)= :date and m.bankomat_id= :bankomatId and m.user_action='BANKOMATNI_TOLDIRISH'" , nativeQuery = true)
    Double getDailyBankomatReplenish(@Param("date") Date date, @Param("bankomatId") Integer bankomatId);

    @Query(value = "select amount || ' ' || currency || 'lik - '  || count || ' dona' as hisobot\n" +
            "from bankomat_kupyura bk join kupyura k on bk.kupyura_id = k.id\n" +
            "where bk.bankomat_id= :bankomatId", nativeQuery = true)
    List<String> getKupyurasInBankomat( @Param("bankomatId") Integer bankomatId);
}
