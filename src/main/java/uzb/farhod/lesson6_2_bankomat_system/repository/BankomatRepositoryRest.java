package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson6_2_bankomat_system.entity.Bankomat;

@RepositoryRestResource(path="bankomat")
public interface BankomatRepositoryRest extends JpaRepository<Bankomat, Integer> {
}
