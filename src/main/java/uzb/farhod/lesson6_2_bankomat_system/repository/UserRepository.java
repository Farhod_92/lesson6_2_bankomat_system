package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_2_bankomat_system.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByEmail(String email);
    Optional<User> findByEmail(String email);
 }
