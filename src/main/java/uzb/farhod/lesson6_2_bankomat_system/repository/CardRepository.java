package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_2_bankomat_system.entity.Card;

import java.util.Optional;

public interface CardRepository extends JpaRepository<Card, Integer> {
    boolean existsByCardNumber(String number);
    Optional<Card> findByCardNumber(String number);
}
