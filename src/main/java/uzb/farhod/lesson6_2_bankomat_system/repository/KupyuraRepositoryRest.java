package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson6_2_bankomat_system.entity.Kupyura;

@RepositoryRestResource(path = "kupyura")
public interface KupyuraRepositoryRest extends JpaRepository<Kupyura, Integer> {
}
