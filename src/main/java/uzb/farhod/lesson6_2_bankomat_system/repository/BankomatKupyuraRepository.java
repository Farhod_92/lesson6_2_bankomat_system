package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_2_bankomat_system.entity.Bankomat;
import uzb.farhod.lesson6_2_bankomat_system.entity.BankomatKupyura;
import uzb.farhod.lesson6_2_bankomat_system.entity.Kupyura;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BankomatKupyuraRepository extends JpaRepository<BankomatKupyura, UUID> {
    Optional<BankomatKupyura> findByBankomatAndKupyura(Bankomat bankomat, Kupyura kupyura);
    List<BankomatKupyura> findAllByBankomatOrderByKupyuraAmountDesc(Bankomat bankomat);
}
