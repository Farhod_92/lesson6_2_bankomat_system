package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson6_2_bankomat_system.entity.Address;

import java.util.Date;

@RepositoryRestResource(path = "address")
public interface AddressRepositoryRest extends JpaRepository<Address, Integer> {
}
