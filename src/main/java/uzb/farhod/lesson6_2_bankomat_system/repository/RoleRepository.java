package uzb.farhod.lesson6_2_bankomat_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_2_bankomat_system.entity.Role;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.RoleEnum;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role getByName(RoleEnum roleEnum);
}
