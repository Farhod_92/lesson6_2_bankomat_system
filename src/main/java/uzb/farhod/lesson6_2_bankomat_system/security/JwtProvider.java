package uzb.farhod.lesson6_2_bankomat_system.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import uzb.farhod.lesson6_2_bankomat_system.entity.Role;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@Component
public class JwtProvider {
    private String secretKey="key dsdsadsadsadsadasdasdasdasdasdasdsadsa";
    private long expireAfter=1000*60*60*24*2;

    public String generateToken(String username,Role role){
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expireAfter))
                .claim("role", role)
                .compact();
        return  token;
    }

    public String getUsernameFromToken(String token){
        String username = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return  username;
    }

}
