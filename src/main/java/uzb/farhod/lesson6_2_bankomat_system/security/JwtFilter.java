package uzb.farhod.lesson6_2_bankomat_system.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uzb.farhod.lesson6_2_bankomat_system.entity.Card;
import uzb.farhod.lesson6_2_bankomat_system.repository.CardRepository;
import uzb.farhod.lesson6_2_bankomat_system.service.AuthService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Component
public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthService authService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CardRepository cardRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("Authorization");

        if(token!=null && token.startsWith("Bearer")){
            token=token.substring(7);
            String usernameFromToken = jwtProvider.getUsernameFromToken(token);
            UserDetails userDetails = authService.loadUserByUsername(usernameFromToken);

            if(usernameFromToken!=null){
                Authentication authentication =new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }else if(token!=null && token.startsWith("Basic")){
            token=token.substring(6);
            byte[] decode = Base64
                    .getDecoder()
                    .decode(token);
            String base64Decoded=new String(decode, StandardCharsets.UTF_8);
            String[] split = base64Decoded.split(":");
            String cardNumber=split[0];
            String pinCode=split[1];
            Card card = authService.loadUserByBase64(cardNumber);
            if(passwordEncoder.matches(pinCode, card.getPassword())){
                UsernamePasswordAuthenticationToken authentication=
                        new UsernamePasswordAuthenticationToken(card,null, card.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
                card.setWrongPasswords(0);
                cardRepository.save(card);
            }else {
                if(card.getWrongPasswords()!=null && card.getWrongPasswords().equals(1)){
                    card.setAccountNonLocked(false);
                    cardRepository.save(card);
                }else {
                    card.setWrongPasswords(1);
                    cardRepository.save(card);
                }

            }
        }
        filterChain.doFilter(request,response);
    }
}
