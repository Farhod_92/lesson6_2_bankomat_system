package uzb.farhod.lesson6_2_bankomat_system.config;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import uzb.farhod.lesson6_2_bankomat_system.entity.Card;
import uzb.farhod.lesson6_2_bankomat_system.entity.User;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.RoleEnum;

import java.util.Optional;
import java.util.UUID;

public class Audit implements AuditorAware<UUID> {
    @Override
    public Optional<UUID> getCurrentAuditor() {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        if(authentication!=null
        && authentication.isAuthenticated()
        && !authentication.getPrincipal().equals("anonymousUser")){
            if(authentication.getPrincipal() instanceof Card) {
                Card card = (Card) authentication.getPrincipal();
                return Optional.of(card.getId());
            }else if(authentication.getPrincipal() instanceof User) {
                User user =(User) authentication.getPrincipal();
                return Optional.of(user.getId());
            }
        }
        return Optional.empty();
    }
}
