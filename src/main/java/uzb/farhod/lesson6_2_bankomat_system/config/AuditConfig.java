package uzb.farhod.lesson6_2_bankomat_system.config;

import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@EnableJpaAuditing
public class AuditConfig {
    @Bean
    AuditorAware<UUID> auditorAware(){
        return new Audit();
    }
}
