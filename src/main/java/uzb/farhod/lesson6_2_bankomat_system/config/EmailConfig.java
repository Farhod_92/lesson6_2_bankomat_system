package uzb.farhod.lesson6_2_bankomat_system.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailConfig {
    @Bean
    JavaMailSender javaMailSender(){
        JavaMailSenderImpl mailSender=new JavaMailSenderImpl();
//        mailSender.setHost("smtp.mail.ru");
//        mailSender.setPort(465);
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);
        mailSender.setUsername("fhotamov92@gmail.com");
        mailSender.setPassword("Farhod1992");
        Properties properties=mailSender.getJavaMailProperties();
        properties.put("mail.transport.protocol","smtp");
        properties.put("mail.smtp.auth","true");
        properties.put("mail.smtp.starttls.enable","true");
        properties.put("mail.debug","true");
//        props.put("mail.smtp.host", "smtp.mail.ru");
//        props.put("mail.debug", "true");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.user", "mymail@mail.ru");
//        props.put("mail.smtp.port", 465);
//        props.put("mail.smtp.socketFactory.port", 465);
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.socketFactory.fallback", "false");

        return mailSender;
    }
}
