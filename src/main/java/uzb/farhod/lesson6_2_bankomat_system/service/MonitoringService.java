package uzb.farhod.lesson6_2_bankomat_system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_2_bankomat_system.entity.Bankomat;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.repository.BankomatRepositoryRest;
import uzb.farhod.lesson6_2_bankomat_system.repository.MonitoringRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MonitoringService {
    @Autowired
    MonitoringRepository monitoringRepository;

    @Autowired
    BankomatRepositoryRest bankomatRepository;

    public ApiResponse getDailyIncome(String dateString, Integer bankomatId){
        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(bankomatId);
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);

        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date=df.parse(dateString);
        } catch (ParseException e) {
            return new ApiResponse("sana xato formatda", false);
        }

        Double dailyIncome = monitoringRepository.getDailyIncome(date, bankomatId);
        return new ApiResponse("kunlik tushum (bankomatga) solingan pullar",true,dailyIncome);
    }

    public ApiResponse getDailyOutcome(String dateString, Integer bankomatId){
        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(bankomatId);
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);

        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date=df.parse(dateString);
        } catch (ParseException e) {
            return new ApiResponse("sana xato formatda", false);
        }

        Double dailyOutcome = monitoringRepository.getDailyOutcome(date, bankomatId);
        return new ApiResponse("kunlik chiqim (bankomatdan) yechilgan pullar",true,dailyOutcome);
    }

    public ApiResponse getDailyBankomatReplenish(String dateString, Integer bankomatId){
        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(bankomatId);
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);

        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date=df.parse(dateString);
        } catch (ParseException e) {
            return new ApiResponse("sana xato formatda", false);
        }

        Double dailyBankomatReplenish = monitoringRepository.getDailyBankomatReplenish(date, bankomatId);
        return new ApiResponse("xodim tomonidan bankomatga solingan pullar",true,dailyBankomatReplenish);
    }

    public ApiResponse getKupyurasInBankomat(Integer bankomatId) {
        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(bankomatId);
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);
        Bankomat bankomat= optionalBankomat.get();
        List<String> kupyurasInBankomat = monitoringRepository.getKupyurasInBankomat(bankomatId);
        kupyurasInBankomat.add("bankomat balansi: "+bankomat.getBalance());
        return new ApiResponse("bankomatdagi kupyuralar ro'yxati: ",true,kupyurasInBankomat);
    }
}
