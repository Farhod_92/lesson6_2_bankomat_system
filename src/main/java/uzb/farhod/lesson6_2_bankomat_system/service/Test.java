package uzb.farhod.lesson6_2_bankomat_system.service;

import uzb.farhod.lesson6_2_bankomat_system.entity.BankomatKupyura;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        double amount=2960;
        final List<Double> kupyuras = new ArrayList<>();
        kupyuras.add(1000d);
        kupyuras.add(500d);
        kupyuras.add(200d);
        kupyuras.add(100d);
        kupyuras.add(50d);
        //avval katta kupyuralardan beradi
        for (Double kupyura : kupyuras) {
            while (amount>=kupyura){
                amount-=kupyura;
                System.out.println(amount);
                System.out.println(kupyura);
            }
        }
    }
}
