package uzb.farhod.lesson6_2_bankomat_system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_2_bankomat_system.entity.Bank;
import uzb.farhod.lesson6_2_bankomat_system.entity.Card;
import uzb.farhod.lesson6_2_bankomat_system.entity.Role;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.CardType;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.RoleEnum;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.payload.CardDto;
import uzb.farhod.lesson6_2_bankomat_system.repository.BankRepositoryRest;
import uzb.farhod.lesson6_2_bankomat_system.repository.CardRepository;
import uzb.farhod.lesson6_2_bankomat_system.repository.RoleRepository;

import java.util.Optional;

@Service
public class CardService {
    @Autowired
    CardRepository cardRepository;

    @Autowired
    BankRepositoryRest bankRepositoryRest;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    public ApiResponse addCard(CardDto cardDto){
        boolean existsByCardNumber= cardRepository.existsByCardNumber(cardDto.getCardNumber());
        if(existsByCardNumber)
            return new ApiResponse("bunaqa karta bor", false);

        Optional<Bank> optionalBank = bankRepositoryRest.findById(cardDto.getBankId());
        if(!optionalBank.isPresent())
            return new ApiResponse("bank topilmadi", false);

        CardType cardType = CardType.valueOf(cardDto.getCardType());
        Role role= roleRepository.getByName(RoleEnum.ROLE_CLIENT);

        Card card =new Card();
        card.setCardNumber(cardDto.getCardNumber());
        card.setBank(optionalBank.get());
        card.setCvv(cardDto.getCvv());
        card.setOwnerFullName(cardDto.getOwnerFullName());
        card.setExpireDate(cardDto.getExpireDate());
        card.setPinCode(passwordEncoder.encode(cardDto.getPinCode()));
        card.setCardType(cardType);
        card.setBalance(cardDto.getBalance());
        card.setRole(role);
        cardRepository.save(card);
        return new ApiResponse("card add", true);
    }

}
