package uzb.farhod.lesson6_2_bankomat_system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_2_bankomat_system.entity.*;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.UserAction;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.payload.ReplenishDto;
import uzb.farhod.lesson6_2_bankomat_system.payload.TakeMoneyDto;
import uzb.farhod.lesson6_2_bankomat_system.repository.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BankomatService {
    @Autowired
    BankomatRepositoryRest bankomatRepository;

    @Autowired
    KupyuraRepositoryRest kupyuraRepository;

    @Autowired
    BankomatKupyuraRepository bankomatKupyuraRepository;

    @Autowired
    MonitoringRepository monitoringRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    AuthService authService;

    @Autowired
    JavaMailSender javaMailSender;

    @Transactional
    public ApiResponse replenishBankomat(ReplenishDto replenishDto){
        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(replenishDto.getBankomatId());
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);

        Optional<Kupyura> optionalKupyura = kupyuraRepository.findById(replenishDto.getKupyuraId());
        if(!optionalKupyura.isPresent())
            return new ApiResponse("kupyura topilmadi", false);

        Kupyura kupyura= optionalKupyura.get();

        Bankomat bankomat=optionalBankomat.get();
        bankomat.setBalance(bankomat.getBalance()+(kupyura.getAmount()* replenishDto.getCount()));
        bankomatRepository.save(bankomat);

        BankomatKupyura bankomatKupyura;
        Optional<BankomatKupyura> optionalBankomatKupyura = bankomatKupyuraRepository.findByBankomatAndKupyura(bankomat, kupyura);
        if(optionalBankomatKupyura.isPresent()) {
            bankomatKupyura = optionalBankomatKupyura.get();
            bankomatKupyura.setCount(bankomatKupyura.getCount()+ replenishDto.getCount());
        }
        else {
            bankomatKupyura = new BankomatKupyura();
            bankomatKupyura.setBankomat(bankomat);
            bankomatKupyura.setKupyura(kupyura);
            bankomatKupyura.setCount(replenishDto.getCount());
        }
        bankomatKupyuraRepository.save(bankomatKupyura);

        Monitoring monitoring =new Monitoring();
        monitoring.setBankomat(bankomat);
        monitoring.setUserAction(UserAction.BANKOMATNI_TOLDIRISH);
        monitoring.setKupyura(kupyura);
        monitoring.setKupyuraCount(replenishDto.getCount());
        monitoringRepository.save(monitoring);

        return new ApiResponse("bakomat to'ldirildi", true);
    }

    @Transactional
    public ApiResponse takeMoneyFromCard(TakeMoneyDto takeMoneyDto) {
        Card card = authService.getCurrentCard();
        if(card.getExpireDate().before(new Date()))
            return new ApiResponse("karta muddati tugagan", false);

        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(takeMoneyDto.getBankomatId());
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);
        Bankomat bankomat = optionalBankomat.get();

        if(!bankomat.getCardTypes().contains(card.getCardType()))
            return new ApiResponse("karta turi tog'ri kelmaydi", false);

        Double comission;
        if(bankomat.getBank().equals(card.getBank()))
            comission=bankomat.getCommissionForSelfBankCardToTakeMoney();
        else
            comission=bankomat.getCommissionStrangerBankCardToTakeMoney();

       double amount = takeMoneyDto.getAmount();
       Double amountWithCommission=amount+amount*comission/100;

        if(bankomat.getMaxMoneyToTake()<amount ||
                card.getBalance()<amountWithCommission ||
                bankomat.getBalance()<amount)
            return new ApiResponse("siz buncha mablag' yecholmaysiz", false);

        final List<BankomatKupyura> allByBankomat = bankomatKupyuraRepository.findAllByBankomatOrderByKupyuraAmountDesc(bankomat);
        //avval katta kupyuralardan beradi
        for (BankomatKupyura bankomatKupyura : allByBankomat) {
            int count=0;
            while (amount>=bankomatKupyura.getKupyura().getAmount()){
                if(bankomatKupyura.getCount()<=0 || bankomatKupyura.getCount()<=count)
                    break;
                amount-=bankomatKupyura.getKupyura().getAmount();
                count++;
            }
            if(count==0)
                continue;
            bankomatKupyura.setCount(bankomatKupyura.getCount()-count);
            bankomatKupyuraRepository.save(bankomatKupyura);
            bankomat.setBalance(bankomat.getBalance()-(bankomatKupyura.getKupyura().getAmount()*count));
            bankomatRepository.save(bankomat);
            card.setBalance(card.getBalance()-(bankomatKupyura.getKupyura().getAmount()+bankomatKupyura.getKupyura().getAmount()*count*comission/100));
            cardRepository.save(card);
            Monitoring monitoring=new Monitoring();
            monitoring.setUserAction(UserAction.NAQD_PUL_YECHISH);
            monitoring.setBankomat(bankomat);
            monitoring.setKupyura(bankomatKupyura.getKupyura());
            monitoring.setKupyuraCount(count);
            monitoringRepository.save(monitoring);
        }

        if(amount!=0)
            throw new ArrayIndexOutOfBoundsException("siz buncha mablag' yecholmaysiz! qoldiq= " + amount);

        if(bankomat.getBalance()<bankomat.getMinMoney()){
            boolean sendEmail = sendEmail(bankomat.getReplenisher().getEmail(), bankomat.getAddress().getStreet());
            if(!sendEmail)
                return new ApiResponse("balans kam lekin emailga xabar yuborilmadi", false);
        }

        return new ApiResponse("pul yechildi",true);
    }

    public ApiResponse replenishCard(ReplenishDto replenishDto){
        Card card = authService.getCurrentCard();
        if(card.getExpireDate().before(new Date()))
            return new ApiResponse("karta muddati tugagan", false);

        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(replenishDto.getBankomatId());
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);

        Optional<Kupyura> optionalKupyura = kupyuraRepository.findById(replenishDto.getKupyuraId());
        if(!optionalKupyura.isPresent())
            return new ApiResponse("kupyura topilmadi", false);

        Kupyura kupyura= optionalKupyura.get();

        Bankomat bankomat=optionalBankomat.get();

        Double comission;
        if(bankomat.getBank().equals(card.getBank()))
            comission=bankomat.getCommissionSelfBankCardToReplenishMoney();
        else
            comission=bankomat.getCommissionStrangerBankCardToReplenishMoney();

        double amount=kupyura.getAmount() * replenishDto.getCount();
        double amountWithComission=amount-amount*comission/100;

        card.setBalance(card.getBalance()+amountWithComission);
        cardRepository.save(card);

        bankomat.setBalance(bankomat.getBalance()+amount);
        bankomatRepository.save(bankomat);

        BankomatKupyura bankomatKupyura;
        Optional<BankomatKupyura> optionalBankomatKupyura = bankomatKupyuraRepository.findByBankomatAndKupyura(bankomat, kupyura);
        if(optionalBankomatKupyura.isPresent()) {
            bankomatKupyura = optionalBankomatKupyura.get();
            bankomatKupyura.setCount(bankomatKupyura.getCount()+ replenishDto.getCount());
        }
        else {
            bankomatKupyura = new BankomatKupyura();
            bankomatKupyura.setBankomat(bankomat);
            bankomatKupyura.setKupyura(kupyura);
            bankomatKupyura.setCount(replenishDto.getCount());
        }
        bankomatKupyuraRepository.save(bankomatKupyura);

        Monitoring monitoring =new Monitoring();
        monitoring.setBankomat(bankomat);
        monitoring.setUserAction(UserAction.KARTAGA_PUL_SOLISH);
        monitoring.setKupyura(kupyura);
        monitoring.setKupyuraCount(replenishDto.getCount());
        monitoringRepository.save(monitoring);

        return new ApiResponse("karta to'ldirildi", true);
    }

    @Transactional
    public ApiResponse setMaxGiveMoneyAll(Double amount) {
        List<Bankomat> all = bankomatRepository.findAll();
        for (Bankomat bankomat : all) {
            bankomat.setMaxMoneyToTake(amount);
            bankomatRepository.save(bankomat);
        }
        return new ApiResponse("bankomatlar max yechiladigan pul miqdori o'rnatildi", true);
    }

    public ApiResponse setMaxGiveMoneyOne(Integer id, Double amount) {
        Optional<Bankomat> optionalBankomat = bankomatRepository.findById(id);
        if(!optionalBankomat.isPresent())
            return new ApiResponse("bankomat topilmadi", false);

        final Bankomat bankomat = optionalBankomat.get();
        bankomat.setMaxMoneyToTake(amount);
        bankomatRepository.save(bankomat);
        return new ApiResponse("bankomat max yechiladigan pul miqdori o'rnatildi", true);
    }


    public boolean sendEmail(String sendingEmail, String street){
        try{
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("fhotamov92@gmail.com");
            message.setTo(sendingEmail);
            message.setSubject(street+ "ko'chadagi bankomatda pul kam kam qoldi!");
            message.setText("BAnkomatni to'ldiring");
            javaMailSender.send(message);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
