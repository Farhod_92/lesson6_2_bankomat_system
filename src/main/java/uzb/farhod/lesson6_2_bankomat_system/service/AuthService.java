package uzb.farhod.lesson6_2_bankomat_system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson6_2_bankomat_system.entity.Card;
import uzb.farhod.lesson6_2_bankomat_system.entity.Role;
import uzb.farhod.lesson6_2_bankomat_system.payload.LoginDto;
import uzb.farhod.lesson6_2_bankomat_system.repository.CardRepository;
import uzb.farhod.lesson6_2_bankomat_system.repository.RoleRepository;
import uzb.farhod.lesson6_2_bankomat_system.repository.UserRepository;
import uzb.farhod.lesson6_2_bankomat_system.entity.User;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.RoleEnum;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.payload.UserRegisterDto;
import uzb.farhod.lesson6_2_bankomat_system.security.JwtProvider;
import java.util.Optional;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    CardRepository cardRepository;

    public ApiResponse register(UserRegisterDto userRegisterDto){
        boolean existsByEmail = userRepository.existsByEmail(userRegisterDto.getEmail());
        if(existsByEmail)
            return new ApiResponse("bunaqa user bor", false);

      //  RoleEnum roleEnum = RoleEnum.valueOf(userRegisterDto.getRoleName());
        Role role = roleRepository.getByName(RoleEnum.ROLE_STAFF);

        User user=new User();
        user.setEmail(userRegisterDto.getEmail());
        user.setPassword(passwordEncoder.encode(userRegisterDto.getPassword()));
        user.setRole(role);
        userRepository.save(user);
        return new ApiResponse("user qo'shildi", true);
    }

    public ApiResponse login(LoginDto loginDto){
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword()));
            User user = (User) authentication.getPrincipal();
            String token = jwtProvider.generateToken(loginDto.getEmail(), user.getRole());
            return new ApiResponse("Bearer "+token, true);
        }catch (BadCredentialsException e){
                return new ApiResponse("username yoki parol xato", false);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByEmail(s);
        return optionalUser.orElseThrow(()-> new UsernameNotFoundException("User topilmadi!"));
    }

//    //bu yerda kartalar auth qilinadi
    public Card loadUserByBase64(String cardNumber){
        Card card = cardRepository.findByCardNumber(cardNumber).orElseThrow(() -> new UsernameNotFoundException(cardNumber));
        return card;
    }

    public User getLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        return user;
    }

    public Card getCurrentCard(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Card card = (Card) authentication.getPrincipal();
        return card;
    }


}
