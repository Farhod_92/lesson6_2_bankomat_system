package uzb.farhod.lesson6_2_bankomat_system.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.CardType;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Bankomat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Bank bank;


    @ElementCollection(targetClass = CardType.class)
//    @CollectionTable(name = "bankomat_card_type",
//            joinColumns = @JoinColumn(name = "bankomat_id"))
    @Enumerated(EnumType.STRING)
    private List<CardType> cardTypes;

    private Double maxMoneyToTake;

    private Double commissionStrangerBankCardToTakeMoney;
    private Double commissionStrangerBankCardToReplenishMoney;
    private Double commissionForSelfBankCardToTakeMoney;
    private Double commissionSelfBankCardToReplenishMoney;

    private Double minMoney;

    private Double balance;

    @ManyToOne
    private Address address;

    @ManyToOne
    private User replenisher;
}
