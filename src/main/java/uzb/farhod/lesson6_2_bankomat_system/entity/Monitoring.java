package uzb.farhod.lesson6_2_bankomat_system.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import uzb.farhod.lesson6_2_bankomat_system.entity.enums.UserAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Monitoring {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserAction userAction;

    @ManyToOne
    Bankomat bankomat;

    @ManyToOne
    private Kupyura kupyura;

    private Integer kupyuraCount;

    @CreationTimestamp
    private Timestamp time;

    @CreatedBy
    private UUID userId;
}
