package uzb.farhod.lesson6_2_bankomat_system.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class BankomatKupyura {
    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    private Bankomat bankomat;

    @ManyToOne
    private Kupyura kupyura;

    private Integer count;
}
