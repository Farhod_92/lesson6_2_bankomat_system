package uzb.farhod.lesson6_2_bankomat_system.entity.enums;

public enum CardType {
    HUMO,
    UZCARD,
    VISA
}
