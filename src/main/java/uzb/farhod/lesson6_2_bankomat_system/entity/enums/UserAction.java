package uzb.farhod.lesson6_2_bankomat_system.entity.enums;

public enum UserAction {
    NAQD_PUL_YECHISH,
    KARTAGA_PUL_SOLISH,
    BANKOMATNI_TOLDIRISH
}
