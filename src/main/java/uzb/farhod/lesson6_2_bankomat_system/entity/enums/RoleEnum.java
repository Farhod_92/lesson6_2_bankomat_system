package uzb.farhod.lesson6_2_bankomat_system.entity.enums;

public enum RoleEnum {
    ROLE_DIRECTOR,
    ROLE_STAFF,
    ROLE_CLIENT
}
