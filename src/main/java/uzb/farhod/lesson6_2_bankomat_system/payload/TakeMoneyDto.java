package uzb.farhod.lesson6_2_bankomat_system.payload;

import lombok.Getter;

@Getter
public class TakeMoneyDto {
    private double amount;
    private Integer bankomatId;
}
