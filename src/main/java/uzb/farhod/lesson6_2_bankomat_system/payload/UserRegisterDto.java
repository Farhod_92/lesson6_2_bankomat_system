package uzb.farhod.lesson6_2_bankomat_system.payload;

import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
public class UserRegisterDto {

    @NotNull
    @Email
    private String email;

    @NotNull
    private String password;
}
