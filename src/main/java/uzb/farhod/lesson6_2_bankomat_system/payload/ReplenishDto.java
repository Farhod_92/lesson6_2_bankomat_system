package uzb.farhod.lesson6_2_bankomat_system.payload;

import lombok.Getter;

@Getter
public class ReplenishDto {
    private Integer bankomatId;
    private Integer kupyuraId;
    private Integer count;
}
