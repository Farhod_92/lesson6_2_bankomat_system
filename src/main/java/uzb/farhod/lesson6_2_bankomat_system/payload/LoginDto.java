package uzb.farhod.lesson6_2_bankomat_system.payload;

import lombok.Getter;

@Getter
public class LoginDto {
    private String email;
    private String password;
}
