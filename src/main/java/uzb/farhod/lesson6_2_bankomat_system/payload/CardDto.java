package uzb.farhod.lesson6_2_bankomat_system.payload;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Getter
public class CardDto {
    @NotBlank
    @Pattern(regexp = "^[0-9]{16}$", message = "cardnumber 16 xonalik sonlar bo'lishi kerak")
    private String cardNumber;

    @NotNull
    private Integer bankId;

    @Pattern(regexp = "^[0-9]{3}$")
    private String cvv;

    @NotNull
    private String ownerFullName;

    @NotNull
    private Date expireDate;

    @Pattern(regexp = "^[0-9]{4}$")
    private String pinCode;

    @NotNull
    private String cardType;

    private double balance;
}
