package uzb.farhod.lesson6_2_bankomat_system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson62BankomatSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson62BankomatSystemApplication.class, args);
    }
    //https://docs.google.com/document/d/1wy01_nKFsRSU53WKRJZJ1vAZQ1GqafnXwsijpk5dWZM/edit?usp=sharing
}
