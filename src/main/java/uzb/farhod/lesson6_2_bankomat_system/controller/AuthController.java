package uzb.farhod.lesson6_2_bankomat_system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.payload.LoginDto;
import uzb.farhod.lesson6_2_bankomat_system.payload.UserRegisterDto;
import uzb.farhod.lesson6_2_bankomat_system.service.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserRegisterDto userRegisterDto){
        ApiResponse register = authService.register(userRegisterDto);
        return ResponseEntity.status(register.isSuccess()?201:409).body(register);
    }

    @GetMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto){
        ApiResponse login = authService.login(loginDto);
        return ResponseEntity.status(login.isSuccess()?201:409).body(login);
    }

    @PreAuthorize(value = "hasRole('ROLE_DIRECTOR')")
    @GetMapping("/test")
    public ResponseEntity<?> test(){
        return ResponseEntity.ok("OK");
    }
}
