package uzb.farhod.lesson6_2_bankomat_system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.payload.CardDto;
import uzb.farhod.lesson6_2_bankomat_system.payload.ReplenishDto;
import uzb.farhod.lesson6_2_bankomat_system.payload.TakeMoneyDto;
import uzb.farhod.lesson6_2_bankomat_system.service.BankomatService;
import uzb.farhod.lesson6_2_bankomat_system.service.CardService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/card")
public class CardController {
    @Autowired
    CardService cardService;

    @Autowired
    BankomatService bankomatService;

    @PostMapping
    public ResponseEntity<?> addCard(@Valid @RequestBody CardDto cardDto){
        ApiResponse apiResponse = cardService.addCard(cardDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping("/get")
    public ResponseEntity<?> takeMoneyFromCard(@RequestBody TakeMoneyDto takeMoneyDto){
        try {
        ApiResponse takeMoneyFromCard=bankomatService.takeMoneyFromCard(takeMoneyDto);
        return ResponseEntity.status(takeMoneyFromCard.isSuccess()?201:409).body(takeMoneyFromCard);
        }catch (ArrayIndexOutOfBoundsException ex) {
            return ResponseEntity.status(409).body(ex.getMessage());
        }
    }

    @PostMapping("/replenish")
    public ResponseEntity<?> replenishCard(@RequestBody ReplenishDto replenishDto){
        ApiResponse replenishCard=bankomatService.replenishCard(replenishDto);
        return ResponseEntity.status(replenishCard.isSuccess()?201:409).body(replenishCard);
    }

    //validatsiya message
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
