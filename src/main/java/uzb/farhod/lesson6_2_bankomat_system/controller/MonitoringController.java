package uzb.farhod.lesson6_2_bankomat_system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.service.MonitoringService;

@PreAuthorize(value = "hasRole('ROLE_DIRECTOR')")
@RestController
@RequestMapping("/api/monitoring")
public class MonitoringController {
    @Autowired
    MonitoringService monitoringService;

    @GetMapping("/daily/income")
    public ResponseEntity<?> getDailyIncome(@RequestParam String date, @RequestParam Integer bankomatId){
        ApiResponse dailyIncome = monitoringService.getDailyIncome(date, bankomatId);
        return ResponseEntity.status(dailyIncome.isSuccess()?200:409).body(dailyIncome);
    }

    @GetMapping("/daily/outcome")
    public ResponseEntity<?> getDailyOutcome(@RequestParam String date, @RequestParam Integer bankomatId){
        ApiResponse dailyIncome = monitoringService.getDailyOutcome(date, bankomatId);
        return ResponseEntity.status(dailyIncome.isSuccess()?200:409).body(dailyIncome);
    }

    @GetMapping("/daily/replenishBankomat")
    public ResponseEntity<?> getDailyBankomatReplenish(@RequestParam String date, @RequestParam Integer bankomatId){
        ApiResponse dailyIncome = monitoringService.getDailyBankomatReplenish(date, bankomatId);
        return ResponseEntity.status(dailyIncome.isSuccess()?200:409).body(dailyIncome);
    }

    @GetMapping("/kupyurasInBankomat/{bankomatId}")
    public ResponseEntity<?> getKupyurasInBankomat( @PathVariable Integer bankomatId){
        ApiResponse dailyIncome = monitoringService.getKupyurasInBankomat( bankomatId);
        return ResponseEntity.status(dailyIncome.isSuccess()?200:409).body(dailyIncome);
    }
}
