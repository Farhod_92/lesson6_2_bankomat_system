package uzb.farhod.lesson6_2_bankomat_system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_2_bankomat_system.payload.ApiResponse;
import uzb.farhod.lesson6_2_bankomat_system.payload.ReplenishDto;
import uzb.farhod.lesson6_2_bankomat_system.service.BankomatService;

@PreAuthorize(value = "hasAnyRole('ROLE_STAFF','ROLE_DIRECTOR')")
@RestController
@RequestMapping("/api/bankomat")
public class BankomatController {
    @Autowired
    BankomatService bankomatService;

    @PostMapping("/replenish")
    public ResponseEntity<?> replenish(@RequestBody ReplenishDto replenishDto){
        ApiResponse replenish = bankomatService.replenishBankomat(replenishDto);
        return ResponseEntity.status(replenish.isSuccess()?201:409).body(replenish);
    }

    @PreAuthorize(value = "hasRole('ROlE_DIRECTOR')")
    @PostMapping("/setMaxGiveMoney")
    public ResponseEntity<?> setMaxGiveMoneyAll(@RequestParam Double amount){
        ApiResponse setMaxGiveMoney = bankomatService.setMaxGiveMoneyAll(amount);
        return ResponseEntity.status(setMaxGiveMoney.isSuccess()?201:409).body(setMaxGiveMoney);
    }

    @PreAuthorize(value = "hasRole('ROlE_DIRECTOR')")
    @PostMapping("/setMaxGiveMoney/{id}")
    public ResponseEntity<?> setMaxGiveMoneyOne(@PathVariable Integer id, @RequestParam Double amount){
        ApiResponse setMaxGiveMoney = bankomatService.setMaxGiveMoneyOne(id, amount);
        return ResponseEntity.status(setMaxGiveMoney.isSuccess()?201:409).body(setMaxGiveMoney);
    }

}
