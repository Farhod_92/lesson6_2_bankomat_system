insert into role(name) values ('ROLE_DIRECTOR'),('ROLE_STAFF'),('ROLE_CLIENT');

insert into usr(id, account_non_expired, account_non_locked, credentials_non_expired, email, enabled, password, role_id)
values ('a215434a-01bc-11ec-9a03-0242ac130003',
        true,
        true,
        true,
        'root',
        true,
        '$2a$10$eoMcclSxxvWldc215xnC8e0jIDqAa17Rd/15w/MRn2yo0/P3c1YrG',        --parol=123
        1);

insert into kupyura(amount, currency) values ('1000','SOM'),
                                           ('5000','SOM'),
                                           ('10000','SOM'),
                                           ('50000','SOM'),
                                           ('100000','SOM'),
                                           ('1','DOLLAR'),
                                           ('5','DOLLAR'),
                                           ('10','DOLLAR'),
                                           ('20','DOLLAR'),
                                           ('50','DOLLAR'),
                                           ('100','DOLLAR');
